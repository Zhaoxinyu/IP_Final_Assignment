import arcpy
from arcpy.sa import *
# input data set (feature class, shapefile)
inputFC = arcpy.GetParameterAsText(0)
arcpy.CheckOutExtension("spatial")
#Fill
outfill=Fill(inputFC)
#FlowDirection
outFlowDirection=FlowDirection(outfill)
#FlowAccumulation
outFlowAccumulation=FlowAccumulation(outFlowDirection)

#water accumulation threshold
threshold=arcpy.GetParameterAsText(1)
outGreatThan=GreaterThan(outFlowAccumulation,threshold)

#output waternet
outputwaternetpath=arcpy.GetParameterAsText(2)
#raster convert to shp
arcpy.RasterToPolyline_conversion(outGreatThan,outputwaternetpath,"ZERO",50,"SIMPLIFY")